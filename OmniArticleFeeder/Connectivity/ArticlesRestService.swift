//
// Created by Łukasz Sypniewski on 2019-04-14.
// Copyright (c) 2019 Łukasz Sypniewski. All rights reserved.
//

import Foundation
import Alamofire

protocol ArticlesRestService {
    func fetch(completion: @escaping (Result<ArticleList>) -> Void)
}

struct OmniArticlesRestService: ArticlesRestService {

    private let url: URL! = URL(string: "http://omni-content.omni.news/search")
    private let apiClient: ApiClient
    private let queryValue: String

    init(apiClient: ApiClient, queryValue: String) {
        self.apiClient = apiClient
        self.queryValue = queryValue
    }

    func fetch(completion: @escaping (Result<ArticleList>) -> Void) {
        apiClient.fetchDecodable(url: url, parameters: ["query": queryValue], headers: [:], completion: completion)
    }
}
