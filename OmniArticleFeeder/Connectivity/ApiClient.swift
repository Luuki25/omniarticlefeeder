//
// Created by Łukasz Sypniewski on 2019-04-14.
// Copyright (c) 2019 Łukasz Sypniewski. All rights reserved.
//

import Foundation
import Alamofire

protocol ApiClient {
    func fetchDecodable<T: Decodable>(url: URL, parameters: [String: String], headers: [String: String],
                                      completion: @escaping (Result<T>) -> Void)
}

struct AlamofireApiClient: ApiClient {

    static let shared: AlamofireApiClient = AlamofireApiClient()

    private init() {}

    func fetchDecodable<T: Decodable>(url: URL, parameters: [String: String], headers: [String: String],
                                      completion: @escaping (Result<T>) -> Void) {

        Alamofire.request(url, method: .get, parameters: parameters)
                .responseData { response in
                    switch response.result {
                    case .success(let data):
                        guard let decodedEntity = try? JSONDecoder().decode(T.self, from: data) else {
                            completion(Result.failure(error: ConnectivityError.mappingError))
                            return
                        }
                        completion(Result.success(value: decodedEntity))
                    case .failure(let error): completion(Result.failure(error: error))
                    }
                }
    }
}
