//
//  AppDelegate.swift
//  OmniArticleFeeder
//
//  Created by Łukasz Sypniewski on 13-04-2019.
//  Copyright © 2019 Łukasz Sypniewski. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)

        let rootViewController = EntryViewController(nibName: nil, bundle: nil)
        rootViewController.view.backgroundColor = .blue
        window!.rootViewController = rootViewController
        window!.makeKeyAndVisible()
        return true
    }
}

