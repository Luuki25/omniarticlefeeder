//
// Created by Łukasz Sypniewski on 2019-04-14.
// Copyright (c) 2019 Łukasz Sypniewski. All rights reserved.
//

import Foundation

struct ResourceTopic: Decodable, Equatable, Hashable {
    let topic: String
    let type: String
}
