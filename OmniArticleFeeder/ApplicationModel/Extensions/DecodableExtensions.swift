//
// Created by Łukasz Sypniewski on 2019-04-14.
// Copyright (c) 2019 Łukasz Sypniewski. All rights reserved.
//

import Foundation

extension Article: Decodable {

    static let emptyTitle = "Title not available"

    enum CodingKeys: String, CodingKey, Equatable {
        case articleId = "article_id"
        case title
        case value
        case resources
        case mainText = "main_text"
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        let titleContainer = try? container.nestedContainer(keyedBy: CodingKeys.self, forKey: .title)

        title = try titleContainer?.decodeIfPresent(String.self, forKey: .value) ?? Article.emptyTitle

        let resources = try container.decode([Resource].self, forKey: .resources)

        imageId = resources.compactMap { $0.image }.first?.id

        topic = resources.compactMap { $0.topic }.first

        mainText = try container.decodeIfPresent(MainText.self, forKey: .mainText) ?? MainText.empty
    }
}

extension Resource: Decodable {

    enum CodingKeys: String, CodingKey, Equatable {
        case imageAsset = "image_asset"
        case id
        case topic
        case title
        case type
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        let imageAssetContainerContainer = try? container.nestedContainer(keyedBy: CodingKeys.self, forKey: .imageAsset)
        let imageID = try imageAssetContainerContainer?.decodeIfPresent(String.self, forKey: .id)
        image = ResourceImage(id: imageID)

        let topicNestedContainer = try? container.nestedContainer(keyedBy: CodingKeys.self, forKey: .topic)
        let topicValue = try topicNestedContainer?.decodeIfPresent(String.self, forKey: .title)
        let topicType = try topicNestedContainer?.decodeIfPresent(String.self, forKey: .type)
        topic = ResourceTopic(topic: topicValue, type: topicType)
    }
}

extension Paragraph: Decodable {

    enum CodingKeys: String, CodingKey, Equatable {
        case text
        case value
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let textContainer = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .text)
        paragraphText = try textContainer.decode(String.self, forKey: .value)
    }
}

extension ResourceImage {
    init?(id: String?) {
        guard let id = id else { return nil }
        self.id = id
    }
}

extension ResourceTopic {
    init?(topic: String?, type: String?) {
        guard let topic = topic,
              let type = type else { return nil }
        self.topic = topic
        self.type = type
    }
}


extension MainText {
    private static let emptyMainText = "Main text is not available"
    static let empty = MainText(paragraphs: [Paragraph(paragraphText: emptyMainText)])
}
