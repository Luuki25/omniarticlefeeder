//
// Created by Łukasz Sypniewski on 2019-04-13.
// Copyright (c) 2019 Łukasz Sypniewski. All rights reserved.
//

import Foundation

struct Article: Equatable {
    let title: String
    let topic: ResourceTopic?
    let imageId: String?
    let mainText: MainText
}
