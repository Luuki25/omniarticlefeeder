//
// Created by Łukasz Sypniewski on 2019-04-15.
// Copyright (c) 2019 Łukasz Sypniewski. All rights reserved.
//

import Foundation
import UIKit

class EntryViewController: UIViewController {

    var articlesScreenDelegate = DefaultListScreenViewModelDelegate()
    var topicsScreenDelegate = DefaultListScreenViewModelDelegate()

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        view.backgroundColor = UIColor.background

        let tabBarController = UITabBarController()
        let controllers = createViewControllers()

        setTabBarController(tabBarController, controllers: controllers)
        present(tabBarController, animated: true)
    }

    private func setTabBarController(_ tabBarController: UITabBarController,
                                     controllers: [UIViewController]) {
        tabBarController.tabBar.tintColor = UIColor.barAccent
        tabBarController.tabBar.barTintColor = UIColor.accent

        let prefersLargeTitles = CurrentDevice.type == .pad

        tabBarController.viewControllers = controllers.map {
            let navigationController = UINavigationController(rootViewController: $0)
            navigationController.navigationBar.prefersLargeTitles = prefersLargeTitles
            navigationController.navigationBar.barTintColor = UIColor.accent
            navigationController.navigationBar.tintColor = UIColor.barAccent

            navigationController.navigationBar.largeTitleTextAttributes =
                    [NSAttributedString.Key.foregroundColor: UIColor.accentFont]

            if !prefersLargeTitles {
                navigationController.navigationBar.titleTextAttributes =
                        [
                            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.semibold),
                            NSAttributedString.Key.foregroundColor: UIColor.accentFont
                        ]
            }
            return navigationController
        }
    }

    private func createViewControllers() -> [UIViewController] {
        let tabBarImage = UIImage(named: "tabIcon.png") ?? UIImage()

        let queryValue = "stockholm"

        let articlesListViewControllersFactory = DefaultListScreenViewControllersFactory(queryValue: queryValue,
                title: "Articles",
                tabBarImage: tabBarImage,
                viewModelFactory: DefaultListScreenViewModelFactory(listContentType: .articles),
                cellViewModelFactory: ArticlesListScreenCellViewModelsFactory(),
                tabBarTag: 0)
        let articleListViewController = articlesListViewControllersFactory.build(delegate: articlesScreenDelegate)

        let topicsListViewControllersFactory = DefaultListScreenViewControllersFactory(queryValue: queryValue,
                title: "Topics",
                tabBarImage: tabBarImage,
                viewModelFactory: DefaultListScreenViewModelFactory(listContentType: .topics),
                cellViewModelFactory: TopicsListScreenCellViewModelsFactory(),
                tabBarTag: 1)
        let topicsListViewController = topicsListViewControllersFactory.build(delegate: topicsScreenDelegate)

        return [articleListViewController, topicsListViewController]
    }
}
