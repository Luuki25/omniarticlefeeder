//
// Created by Łukasz Sypniewski on 2019-04-17.
// Copyright (c) 2019 Łukasz Sypniewski. All rights reserved.
//

import UIKit

struct CurrentDevice {
    static var type: UIUserInterfaceIdiom { return UIDevice.current.userInterfaceIdiom }
}
