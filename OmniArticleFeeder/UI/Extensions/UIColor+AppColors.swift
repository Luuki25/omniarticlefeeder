//
// Created by Łukasz Sypniewski on 2019-04-16.
// Copyright (c) 2019 Łukasz Sypniewski. All rights reserved.
//

import UIKit

extension UIColor {
    static let main: UIColor = UIColor(red: 180 / 255, green: 41 / 255, blue: 128 / 255, alpha: 1)
    static let accent: UIColor = UIColor(red: 130 / 255, green: 30 / 255, blue: 93 / 255, alpha: 1)
    static let cellSelection: UIColor = UIColor(red: 207 / 255, green: 117 / 255, blue: 255 / 255, alpha: 1)
    static let background: UIColor = UIColor(red: 193 / 255, green: 102 / 255, blue: 160 / 255, alpha: 1)
    static let mainFont: UIColor = UIColor(red: 208 / 255, green: 221 / 255, blue: 241 / 255, alpha: 1)
    static let accentFont: UIColor = UIColor(red: 208 / 255, green: 221 / 255, blue: 241 / 255, alpha: 1)
    static let barAccent: UIColor = UIColor(red: 193 / 255, green: 175 / 255, blue: 64 / 255, alpha: 1)
}
