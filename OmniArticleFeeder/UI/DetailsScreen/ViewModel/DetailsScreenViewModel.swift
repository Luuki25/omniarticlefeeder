//
// Created by Łukasz Sypniewski on 2019-04-15.
// Copyright (c) 2019 Łukasz Sypniewski. All rights reserved.
//

import Foundation

protocol DetailsScreenViewModel {
    var text: String { get }
    var listContentType: ListContentType { get }
}

struct DefaultDetailsScreenViewModel: DetailsScreenViewModel {
    let text: String
    let listContentType: ListContentType

    init(text: String, listContentType: ListContentType) {
        self.text = text.replacingOccurrences(of: "\n", with: "\n\n")
        self.listContentType = listContentType
    }
}
