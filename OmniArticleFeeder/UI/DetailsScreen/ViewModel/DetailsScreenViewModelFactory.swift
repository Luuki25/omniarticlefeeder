//
// Created by Łukasz Sypniewski on 2019-04-16.
// Copyright (c) 2019 Łukasz Sypniewski. All rights reserved.
//

import Foundation

protocol DetailsScreenViewModelFactory {
    func build() -> DetailsScreenViewModel
}

struct DefaultDetailsScreenViewModelFactory: DetailsScreenViewModelFactory {

    private let listScreenViewModel: ListScreenViewModel
    private let selectedIndex: Int

    init(listScreenViewModel: ListScreenViewModel, selectedIndex: Int) {
        self.listScreenViewModel = listScreenViewModel
        self.selectedIndex = selectedIndex
    }

    func build() -> DetailsScreenViewModel {
        switch listScreenViewModel.listContentType {
        case .articles: return articlesDetailsScreenViewModel()
        case .topics: return topicsDetailsScreenViewModel()
        }
    }

    private func articlesDetailsScreenViewModel() -> DetailsScreenViewModel {
        let paragraphs = listScreenViewModel.articlesListData[selectedIndex].mainText.paragraphs
        let text = paragraphs.map { paragraph in
            paragraph.paragraphText
        }
        let separatedText = text.joined(separator: "\n")
        return DefaultDetailsScreenViewModel(text: separatedText, listContentType: listScreenViewModel.listContentType)
    }

    private func topicsDetailsScreenViewModel() -> DetailsScreenViewModel {
        let topics = listScreenViewModel.topicsListData[selectedIndex].type
        return DefaultDetailsScreenViewModel(text: "Topic type:\n\(topics)", listContentType: listScreenViewModel.listContentType)
    }
}
