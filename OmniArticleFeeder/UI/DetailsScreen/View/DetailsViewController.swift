//
// Created by Łukasz Sypniewski on 2019-04-16.
// Copyright (c) 2019 Łukasz Sypniewski. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class DetailsViewController: UIViewController {

    private let containerView: UIView
    private let scrollView: UIScrollView
    private let label: UILabel
    private let viewModel: DetailsScreenViewModel

    private let scrollViewSideInset = UIScreen.main.bounds.width * 0.05

    init(viewModel: DetailsScreenViewModel) {
        self.viewModel = viewModel
        containerView = UIView()
        label = UILabel()
        scrollView = UIScrollView()

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) { fatalError("init?(coder aDecoder: NSCoder) has not been implemented") }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.main
        setControls()
    }

    private func setControls() {
        setScrollView()
        setContainerLabel()
        setLabel()
    }

    private func setScrollView() {
        view.addSubview(scrollView)
        setInitialScrollViewConstraints()
    }

    private func setContainerLabel() {
        scrollView.addSubview(containerView)
        containerView.snp.makeConstraints { maker in
            maker.top.bottom.width.equalToSuperview()
        }
    }

    private func setInitialScrollViewConstraints() {
        scrollView.snp.remakeConstraints { maker in
            maker.leading.trailing.equalToSuperview()
            maker.top.equalTo(-scrollView.intrinsicContentSize.height)
            maker.height.equalTo(scrollView.snp.height)
        }
        view.setNeedsUpdateConstraints()
        view.layoutIfNeeded()
    }

    private func setLabel() {
        containerView.addSubview(label)

        label.snp.remakeConstraints { maker in
            maker.top.bottom.equalToSuperview()
            maker.leading.trailing.equalToSuperview().inset(scrollViewSideInset)
        }

        scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: label.intrinsicContentSize.height)
        view.setNeedsUpdateConstraints()
        view.layoutIfNeeded()

        label.textColor = UIColor.mainFont
        label.textAlignment = textAlignment(for: viewModel)
        label.numberOfLines = 0
        label.text = viewModel.text
        label.font = UIFont.systemFont(ofSize: fontSize(), weight: UIFont.Weight.regular)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showScrollView()
    }

    private func showScrollView() {
        scrollView.snp.remakeConstraints { maker in
            maker.trailing.leading.equalToSuperview()
            maker.top.equalTo(view.safeAreaLayoutGuide.snp.top).inset(8)
            maker.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
        }
        self.view.setNeedsUpdateConstraints()

        UIView.animate(withDuration: 0.5, animations: { [unowned self] in
             self.view.layoutIfNeeded()
            self.scrollView.contentOffset = CGPoint(x: 0, y: -60)
        }, completion: { _ in
            UIView.animate(withDuration: 0.5, animations: {  [unowned self] in
                 self.scrollView.contentOffset = CGPoint(x: 0, y: 0)
            })
        })

    }

    private func textAlignment(for viewModel: DetailsScreenViewModel) -> NSTextAlignment {
        switch viewModel.listContentType {
        case .articles: return .justified
        case .topics: return .center
        }
    }

    private func fontSize() -> CGFloat {
        switch CurrentDevice.type {
        case .pad: return 35
        default: return 17
        }
    }
}
