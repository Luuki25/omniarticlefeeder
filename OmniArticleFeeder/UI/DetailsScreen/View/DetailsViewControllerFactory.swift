//
// Created by Łukasz Sypniewski on 2019-04-16.
// Copyright (c) 2019 Łukasz Sypniewski. All rights reserved.
//

import Foundation

protocol DetailsViewControllerFactory {
    func build() -> DetailsViewController
}

struct DefaultDetailsViewControllerFactory: DetailsViewControllerFactory {

    private let listScreenViewModel: ListScreenViewModel
    private let selectedIndex: Int

    init(listScreenViewModel: ListScreenViewModel, selectedIndex: Int) {
        self.listScreenViewModel = listScreenViewModel
        self.selectedIndex = selectedIndex
    }

    func build() -> DetailsViewController {
        let detailsScreenViewModelFactory = DefaultDetailsScreenViewModelFactory(listScreenViewModel: listScreenViewModel,
                selectedIndex: selectedIndex)
        let detailsViewModel = detailsScreenViewModelFactory.build()
        let detailsViewController = DetailsViewController(viewModel: detailsViewModel)
        detailsViewController.title = detailsViewControllerTitle(for: listScreenViewModel, index: selectedIndex)
        return detailsViewController
    }

    private func detailsViewControllerTitle(for viewModel: ListScreenViewModel,
                                            index: Int) -> String {
        switch viewModel.listContentType {
        case .articles: return titleForArticlesDetailsViewController(articleListData: viewModel.articlesListData, for: index)
        case .topics: return titleForTopicsDetailsViewController(topicsListData: viewModel.topicsListData, for: index)
        }
    }

    private func titleForArticlesDetailsViewController(articleListData: [ArticleListData], for selectedIndex: Int) -> String {
        guard selectedIndex < articleListData.count else { return "Article title is not available" }
        return articleListData[selectedIndex].title
    }

    private func titleForTopicsDetailsViewController(topicsListData: [ResourceTopic], for selectedIndex: Int) -> String {
        guard selectedIndex < topicsListData.count else { return "Topic name is not available" }
        return topicsListData[selectedIndex].topic
    }
}
