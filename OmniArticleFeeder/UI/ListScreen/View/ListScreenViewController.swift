//
// Created by Łukasz Sypniewski on 2019-04-15.
// Copyright (c) 2019 Łukasz Sypniewski. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import AlamofireImage

class ListScreenViewController: UIViewController {

    private var cellViewModels: [ListScreenCellViewModel] = []

    private let viewModel: ListScreenViewModel
    private let listScreenTableViewCell = "listScreenTableViewCell"
    private let tableView: UITableView
    private let cellViewModelsFactory: ListScreenCellViewModelsFactory
    private let refreshControl = UIRefreshControl()

    init(viewModel: ListScreenViewModel, cellViewModelsFactory: ListScreenCellViewModelsFactory) {
        self.viewModel = viewModel
        self.tableView = UITableView()
        self.cellViewModelsFactory = cellViewModelsFactory
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) { fatalError("init?(coder aDecoder: NSCoder) has not been implemented") }

    override func viewDidLoad() {

        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        super.viewDidLoad()
        view.backgroundColor = UIColor.background
        setTableView()
        setConstraints()
    }

    @objc private func refreshData() {
        viewModel.loadData(onSuccess: { [unowned self] in
            self.tableView.isHidden = true
            self.cellViewModels = self.cellViewModelsFactory.build(from: self.viewModel)
            self.tableView.reloadData()
            self.tableView.isHidden = false
            self.refreshControl.endRefreshing()
        }, onFailure: nil)
    }

    private func setTableView() {
        tableView.backgroundColor = UIColor.background
        tableView.isHidden = true
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "ListScreenCell", bundle: nil), forCellReuseIdentifier: listScreenTableViewCell)
        tableView.refreshControl = refreshControl
    }

    private func setConstraints() {
        setTableViewConstraints()
    }

    private func setTableViewConstraints() {
        view.addSubview(tableView)
        tableView.snp.makeConstraints { maker in
            maker.edges.equalToSuperview()
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        refreshData()
    }
}

extension ListScreenViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let screenHeight = UIScreen.main.bounds.size.height
        switch UIDevice.current.orientation {
        case .landscapeLeft, .landscapeRight: return screenHeight * 0.15
        default: return screenHeight * 0.2
        }
    }

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.delegate?.didTapCell(viewModel,
                navigationController: navigationController,
                at: indexPath.row)
    }
}

extension ListScreenViewController: UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellViewModels.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: listScreenTableViewCell, for: indexPath) as! ListScreenCell
        let cellViewModel = cellViewModels[indexPath.row]
        cell.configure(with: cellViewModel)

        cell.selectionStyle = .default
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.cellSelection
        cell.selectedBackgroundView = bgColorView

        guard let imageURLString = cellViewModel.imageURL,
              let imageURL = URL(string: imageURLString) else {
            cell.articleImageView.image = cellViewModel.placeholderImage
            return cell
        }

        fetchImages(for: cell, from: imageURL, placeholder: cellViewModel.placeholderImage)
        return cell
    }

    private func fetchImages(for cell: ListScreenCell, from url: URL, placeholder: UIImage) {
        cell.articleImageView.af_setImage(
                withURL: url,
                placeholderImage: placeholder,
                filter: RoundedCornersFilter(radius: 20.0),
                imageTransition: UIImageView.ImageTransition.crossDissolve(0.5),
                runImageTransitionIfCached: false) {
            response in
            if response.response != nil {
                self.tableView.beginUpdates()
                self.tableView.endUpdates()
            }
        }
    }
}
