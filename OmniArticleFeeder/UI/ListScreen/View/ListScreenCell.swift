//
//  ListScreenCell.swift
//  OmniArticleFeeder
//
//  Created by Łukasz Sypniewski on 15-04-2019.
//  Copyright © 2019 Łukasz Sypniewski. All rights reserved.
//

import UIKit
import AlamofireImage

class ListScreenCell: UITableViewCell {

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet weak var articleImageView: UIImageView!

    func configure(with viewModel: ListScreenCellViewModel) {
        titleLabel.textColor = UIColor.mainFont
        backgroundColor = UIColor.main
        titleLabel.text = viewModel.text

        switch CurrentDevice.type {
        case .pad: titleLabel.font = UIFont.systemFont(ofSize: 25, weight: UIFont.Weight.semibold)
        default: titleLabel.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.semibold)
        }
    }
}
