//
// Created by Łukasz Sypniewski on 2019-04-16.
// Copyright (c) 2019 Łukasz Sypniewski. All rights reserved.
//

import Foundation
import UIKit

protocol ListScreenViewControllersFactory {
    func build(delegate: ListScreenViewModelDelegate?) -> ListScreenViewController
}

struct DefaultListScreenViewControllersFactory: ListScreenViewControllersFactory {

    private let queryValue: String
    private let title: String
    private let tabBarImage: UIImage
    private let viewModelFactory: ListScreenViewModelFactory
    private let cellViewModelsFactory: ListScreenCellViewModelsFactory
    private let tabBarTag: Int

    init(queryValue: String, title: String, tabBarImage: UIImage,
         viewModelFactory: ListScreenViewModelFactory,
         cellViewModelFactory: ListScreenCellViewModelsFactory,
         tabBarTag: Int) {
        self.queryValue = queryValue
        self.title = title
        self.tabBarImage = tabBarImage
        self.viewModelFactory = viewModelFactory
        self.cellViewModelsFactory = cellViewModelFactory
        self.tabBarTag = tabBarTag
    }

    func build(delegate: ListScreenViewModelDelegate?) -> ListScreenViewController {
        var viewModel = viewModelFactory.build(queryValue: queryValue)
        viewModel.delegate = delegate

        let listViewController = ListScreenViewController(viewModel: viewModel,
                cellViewModelsFactory: cellViewModelsFactory)
        listViewController.title = title
        listViewController.tabBarItem = UITabBarItem(title: title, image: tabBarImage, tag: tabBarTag)
        return listViewController
    }
}
