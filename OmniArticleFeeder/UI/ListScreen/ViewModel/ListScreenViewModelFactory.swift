//
// Created by Łukasz Sypniewski on 2019-04-16.
// Copyright (c) 2019 Łukasz Sypniewski. All rights reserved.
//

import Foundation

protocol ListScreenViewModelFactory {
    func build(queryValue: String) -> ListScreenViewModel
}

struct DefaultListScreenViewModelFactory: ListScreenViewModelFactory {

    private var listContentType: ListContentType

    init(listContentType: ListContentType) {
        self.listContentType = listContentType
    }

    func build(queryValue: String) -> ListScreenViewModel {
        let apiClient = AlamofireApiClient.shared
        let articlesRestService = OmniArticlesRestService(apiClient: apiClient, queryValue: queryValue)

        return DefaultListScreenViewModel(listContentType: listContentType, articlesRestService: articlesRestService)
    }
}
