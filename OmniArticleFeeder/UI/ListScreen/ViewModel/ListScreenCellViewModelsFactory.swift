//
// Created by Łukasz Sypniewski on 2019-04-15.
// Copyright (c) 2019 Łukasz Sypniewski. All rights reserved.
//

import Foundation
import UIKit

protocol ListScreenCellViewModelsFactory {
    func build(from listScreenViewModel: ListScreenViewModel) -> [ListScreenCellViewModel]
}

struct ArticlesListScreenCellViewModelsFactory: ListScreenCellViewModelsFactory {
    private let baseImageUrl = "https://gfx-ios.omni.se/images/"

    func build(from listScreenViewModel: ListScreenViewModel) -> [ListScreenCellViewModel] {
        return listScreenViewModel.articlesListData.map { articleData in
            let imageURL: String? = articleData.imageID == nil ? nil : "\(baseImageUrl)\(articleData.imageID!)"
            return DefaultListScreenCellViewModel(title: articleData.title,
                    imageURL: imageURL,
                    placeholderImage: UIImage(named: "placeholderArticle.png") ?? UIImage())
        }
    }
}

struct TopicsListScreenCellViewModelsFactory: ListScreenCellViewModelsFactory {

    func build(from listScreenViewModel: ListScreenViewModel) -> [ListScreenCellViewModel] {
        return listScreenViewModel.topicsListData.map { topicData in
            return DefaultListScreenCellViewModel(title: topicData.topic,
                    imageURL: nil,
                    placeholderImage: UIImage(named: "placeholderTopic.jpg") ?? UIImage())
        }
    }
}
