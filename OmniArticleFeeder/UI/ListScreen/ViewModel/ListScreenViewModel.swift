//
// Created by Łukasz Sypniewski on 2019-04-14.
// Copyright (c) 2019 Łukasz Sypniewski. All rights reserved.
//

import Foundation
import UIKit

typealias ArticleListData = (title: String, imageID: String?, mainText: MainText)

protocol ListScreenViewModelDelegate: class {
    func didTapCell(_ viewModel: ListScreenViewModel,
                    navigationController: UINavigationController?,
                    at index: Int)
}

protocol ListScreenViewModel {
    var delegate: ListScreenViewModelDelegate? { get set }
    var articlesListData: [ArticleListData] { get }
    var topicsListData: [ResourceTopic] { get }
    var listContentType: ListContentType { get }

    func loadData(onSuccess: (() -> Void)?, onFailure: ((Error?) -> Void)?)
}

class DefaultListScreenViewModel: ListScreenViewModel {

    private(set) var articlesListData: [ArticleListData] = []
    private(set) var topicsListData: [ResourceTopic] = []
    private(set) var articles: [Article] = []
    let listContentType: ListContentType

    weak var delegate: ListScreenViewModelDelegate?

    private let articlesRestService: ArticlesRestService

    init(listContentType: ListContentType, articlesRestService: ArticlesRestService) {
        self.listContentType = listContentType
        self.articlesRestService = articlesRestService
    }

    func loadData(onSuccess: (() -> Void)? = nil, onFailure: ((Error?) -> Void)? = nil) {
        articlesRestService.fetch(completion: { [weak self] result in
            guard let self = self else { return }

            switch result {
            case .success(let articleList):
                self.articles = articleList.articles
                self.articlesListData = DefaultListScreenViewModel.retrieveArticlesListData(from: self.articles)
                self.topicsListData = DefaultListScreenViewModel.retrieveTopicsListData(from: self.articles)
                onSuccess?()
            case .failure(let error): onFailure?(error)
            }
        })
    }

    private static func retrieveArticlesListData(from articles: [Article]) -> [ArticleListData] {
        return articles.map { article in
            return (article.title, article.imageId, article.mainText)
        }
    }

    private static func retrieveTopicsListData(from articles: [Article]) -> [ResourceTopic] {
        let topics: [ResourceTopic] = articles.compactMap { article in
            return article.topic
        }
        let uniqueTopics = Array(Set(topics))
        return uniqueTopics
    }
}
