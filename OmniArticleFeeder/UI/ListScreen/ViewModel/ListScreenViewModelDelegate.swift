//
// Created by Łukasz Sypniewski on 2019-04-16.
// Copyright (c) 2019 Łukasz Sypniewski. All rights reserved.
//

import Foundation
import UIKit

class DefaultListScreenViewModelDelegate: ListScreenViewModelDelegate {

    func didTapCell(_ viewModel: ListScreenViewModel,
                    navigationController: UINavigationController?,
                    at index: Int) {

        let detailsViewControllerFactory = DefaultDetailsViewControllerFactory(listScreenViewModel: viewModel, selectedIndex: index)
        let detailsViewController = detailsViewControllerFactory.build()
        navigationController?.pushViewController(detailsViewController, animated: true)
    }
}
