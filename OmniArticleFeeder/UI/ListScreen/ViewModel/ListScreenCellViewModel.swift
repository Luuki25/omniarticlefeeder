//
// Created by Łukasz Sypniewski on 2019-04-14.
// Copyright (c) 2019 Łukasz Sypniewski. All rights reserved.
//

import Foundation
import UIKit

protocol ListScreenCellViewModel {
    var text: String { get }
    var imageURL: String? { get }
    var placeholderImage: UIImage { get }
}

struct DefaultListScreenCellViewModel: ListScreenCellViewModel {

    let text: String
    let imageURL: String?
    let placeholderImage: UIImage

    init(title: String, imageURL: String?, placeholderImage: UIImage) {
        self.text = title
        self.imageURL = imageURL
        self.placeholderImage = placeholderImage
    }
}
