//
// Created by Łukasz Sypniewski on 2019-04-17.
// Copyright (c) 2019 Łukasz Sypniewski. All rights reserved.
//

import Mimus
@testable import OmniArticleFeeder

class FakeArticlesRestService: ArticlesRestService, Mock {

    var storage: [RecordedCall] = []
    let idFetch = "idFetch"
    var completionResult: Result<ArticleList> = .success(value: ArticleList(articles: []))

    func fetch(completion: @escaping (Result<ArticleList>) -> Void) {
        recordCall(withIdentifier: idFetch)
        completion(completionResult)
    }
}
