//
// Created by Łukasz Sypniewski on 2019-04-17.
// Copyright (c) 2019 Łukasz Sypniewski. All rights reserved.
//

import Mimus

enum FakeError: Error, MockEquatable {
    case fakeError

    func equalTo(other: Any?) -> Bool {
        return compare(other: other as? FakeError)
    }
}
