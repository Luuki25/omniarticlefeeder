//
// Created by Łukasz Sypniewski on 2019-04-13.
// Copyright (c) 2019 Łukasz Sypniewski. All rights reserved.
//

import Foundation
import Nimble
import Quick
@testable import OmniArticleFeeder

class ArticleMappingSpec: QuickSpec {
    override func spec() {

        describe("ArticleMappingSpec") {

            let mappingArticleSharedExample = "mappingArticleSharedExample"

            sharedExamples(mappingArticleSharedExample) { context in
                var mappedArticle: Article!
                var expectedArticle: Article!

                beforeEach {
                    mappedArticle = context()["mappedArticle"] as? Article
                    expectedArticle = context()["expectedArticle"] as? Article
                }

                it("JSON is correctly mapped") {
                    expect(mappedArticle).to(equal(expectedArticle))
                }
            }

            context("Correct JSON file") {

                var expectedArticle: Article!
                var mappedArticles: ArticleList!

                beforeEach {
                    mappedArticles = JSONLoader.mappedEntity(from: .articleResponse, of: ArticleList.self)
                }

                describe("First article") {

                    beforeEach {
                        let expectedTitle = "Facit: 160 miljarder dollar i Bitcoin gick upp i rök i år"
                        let expectedImageID: String? = nil
                        let expectedParagraph1 = Paragraph(paragraphText: "Efter att ha stigit nästan 1 400 procent under 2017 tappade bitcoin nästan 75 procent.")
                        let expectedParagraph2 = Paragraph(paragraphText: "Det blev ett bryskt uppvaknande för kryptospekulanter, menar Chris Burniske, författare till boken Crytoassets.")
                        let expectedMainText = MainText(paragraphs: [expectedParagraph1, expectedParagraph2])
                        expectedArticle = Article(title: expectedTitle, topic: nil, imageId: expectedImageID, mainText: expectedMainText)

                    }
                    itBehavesLike(mappingArticleSharedExample) {
                        [
                            "mappedArticle": mappedArticles.articles[0],
                            "expectedArticle": expectedArticle
                        ]
                    }
                }

                describe("Second article") {

                    beforeEach {
                        let expectedTitle = "Cannabis vann mark och blev småspararfavorit 2018"
                        let expectedTopic = ResourceTopic(topic: "Cannabisdebatten", type: "story")
                        let expectedImageID = "75ab33af-5393-4a1d-ae67-8229719cb82e"
                        let expectedParagraph1 = Paragraph(paragraphText: "Kanada blev det andra landet att helt legalisera marijuana efter Uruguay i år.")
                        let expectedParagraph2 = Paragraph(paragraphText: "Här är en sammanställning över vad som har hänt i cannabisfrågan under 2018.")
                        let expectedMainText = MainText(paragraphs: [expectedParagraph1, expectedParagraph2])
                        expectedArticle = Article(title: expectedTitle, topic: expectedTopic, imageId: expectedImageID, mainText: expectedMainText)

                    }
                    itBehavesLike(mappingArticleSharedExample) {
                        [
                            "mappedArticle": mappedArticles.articles[1],
                            "expectedArticle": expectedArticle
                        ]
                    }
                }
            }

            context("Incorrect JSON file") {

                var expectedArticle: Article!
                var mappedArticles: ArticleList!

                beforeEach {
                    mappedArticles = JSONLoader.mappedEntity(from: .invalidArticleResponse, of: ArticleList.self)

                    let expectedTitle = Article.emptyTitle
                    let expectedImageID = "c800f994-e844-4712-b270-e52954859210"
                    expectedArticle = Article(title: expectedTitle, topic: nil, imageId: expectedImageID, mainText: MainText.empty)
                }

                itBehavesLike(mappingArticleSharedExample) {
                    [
                        "mappedArticle": mappedArticles.articles[0],
                        "expectedArticle": expectedArticle
                    ]
                }

            }
        }
    }
}
