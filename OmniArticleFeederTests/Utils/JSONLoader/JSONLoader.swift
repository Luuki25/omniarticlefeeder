//
// Created by Łukasz Sypniewski on 2019-04-13.
// Copyright (c) 2019 Łukasz Sypniewski. All rights reserved.
//

import Foundation

struct JSONLoader {

    static func jsonData(from file: JSONFile) -> Data? {
        let fileName = file.rawValue
        let url = Bundle.allBundles.compactMap { $0.url(forResource: fileName, withExtension: "json") }.first
        if let url = url {
            do {
                let data = try Data(contentsOf: url, options: .mappedIfSafe)
                return data
            } catch {
                return nil
            }
        }
        return nil
    }

    static func mappedEntity<T: Decodable>(from file: JSONFile, of type: T.Type) -> T? {
        guard let data = jsonData(from: file) else { return nil }
        return try? JSONDecoder().decode(T.self, from: data)
    }
}
