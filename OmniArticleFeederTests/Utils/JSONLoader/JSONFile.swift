//
// Created by Łukasz Sypniewski on 2019-04-13.
// Copyright (c) 2019 Łukasz Sypniewski. All rights reserved.
//

import Foundation

enum JSONFile: String {
    case articleResponse
    case invalidArticleResponse
}
