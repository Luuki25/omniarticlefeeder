//
// Created by Łukasz Sypniewski on 2019-04-17.
// Copyright (c) 2019 Łukasz Sypniewski. All rights reserved.
//

import Foundation
import Nimble
import Quick
@testable import OmniArticleFeeder

class DefaultListScreenCellViewModelSpec: QuickSpec {
    override func spec() {


        var sut: DefaultListScreenCellViewModel!
        var title: String!
        var imageURL: String!
        var placeholderImage: UIImage!

        describe("Is properly initialized") {

            beforeEach {
                title = "test title"
                imageURL = "test url"
                placeholderImage = UIImage()

                sut = DefaultListScreenCellViewModel(title: title, imageURL: imageURL, placeholderImage: placeholderImage)
            }

            it("title property") {
                expect(sut.text).to(equal(title))
            }

            it("imageURL property") {
                expect(sut.imageURL).to(equal(imageURL))
            }

            it("placeholderImage property") {
                expect(sut.placeholderImage).to(equal(placeholderImage))
            }
        }
    }
}
