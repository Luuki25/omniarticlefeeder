//
// Created by Łukasz Sypniewski on 2019-04-16.
// Copyright (c) 2019 Łukasz Sypniewski. All rights reserved.
//

import Foundation
import Nimble
import Quick
@testable import OmniArticleFeeder

class DefaultDetailsScreenViewModelSpec: QuickSpec {
    override func spec() {

        describe("DefaultDetailsScreenViewModelSpec") {

            it("Adds new line to each paragraph") {
                let inputText = "FirstLine\nSecondLine"
                let expectedViewModelText = "FirstLine\n\nSecondLine"

                let sut = DefaultDetailsScreenViewModel(text: inputText, listContentType: .topics)
                expect(sut.text).to(equal(expectedViewModelText))
            }

            describe("Assigns list content type properly") {

                it("Article type") {
                    let sut = DefaultDetailsScreenViewModel(text: "", listContentType: .articles)
                    expect(sut.listContentType).to(equal(ListContentType.articles))
                }

                it("Topic type") {
                    let sut = DefaultDetailsScreenViewModel(text: "", listContentType: .topics)
                    expect(sut.listContentType).to(equal(ListContentType.topics))
                }
            }
        }
    }
}
