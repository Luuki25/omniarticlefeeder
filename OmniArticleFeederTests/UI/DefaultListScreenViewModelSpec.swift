//
// Created by Łukasz Sypniewski on 2019-04-17.
// Copyright (c) 2019 Łukasz Sypniewski. All rights reserved.
//

import Foundation
import Nimble
import Quick
import Mimus
@testable import OmniArticleFeeder

class DefaultListScreenViewModelSpec: QuickSpec {
    override func spec() {

        var sut: DefaultListScreenViewModel!
        var articlesRestService: FakeArticlesRestService!
        var listContentType: ListContentType!
        var fakeSuccessClosure: FakeClosure!
        var fakeFailureClosure: FakeClosure!

        describe("Is properly initialized") {
            beforeEach {
                articlesRestService = FakeArticlesRestService()
                listContentType = .articles
                fakeSuccessClosure = FakeClosure()
                fakeFailureClosure = FakeClosure()
                sut = DefaultListScreenViewModel(listContentType: listContentType, articlesRestService: articlesRestService)
            }

            it("listContentType property") {
                expect(sut.listContentType).to(equal(ListContentType.articles))
            }
        }

        describe("loadData") {

            beforeEach {
                articlesRestService = FakeArticlesRestService()
                listContentType = .articles
                fakeSuccessClosure = FakeClosure()
                fakeFailureClosure = FakeClosure()
                sut = DefaultListScreenViewModel(listContentType: listContentType, articlesRestService: articlesRestService)
            }

            it("ArticleRestService invoked fetch method") {
                sut.loadData()
                articlesRestService.verifyCall(withIdentifier: articlesRestService.idFetch)
            }

            context("articlesRestService successfully fetched data") {

                beforeEach {
                    articlesRestService.completionResult = .success(value: ArticleList(articles: []))
                    sut.loadData(onSuccess: fakeSuccessClosure.fakeClosure, onFailure: fakeFailureClosure.fakeClosureWithError)
                }

                it("ArticleRestService invokes onSuccess closure when fetching succeeds") {
                    fakeSuccessClosure.verifyCall(withIdentifier: fakeSuccessClosure.idFakeClosure)
                }

                it("ArticleRestService does not invoke onFailure closure when fetching succeeds") {
                    fakeFailureClosure.verifyCall(withIdentifier: fakeFailureClosure.idFakeClosureWithError, mode: .never)
                }

            }

            context("articlesRestService fails to fetch data") {

                beforeEach {
                    articlesRestService.completionResult = .failure(error: FakeError.fakeError)
                    sut.loadData(onSuccess: fakeSuccessClosure.fakeClosure, onFailure: fakeFailureClosure.fakeClosureWithError)
                }

                it("ArticleRestService invokes onFailure closure when fetching fails") {
                    fakeFailureClosure.verifyCall(withIdentifier: fakeFailureClosure.idFakeClosureWithError, arguments: [FakeError.fakeError])
                }

                it("ArticleRestService does not invoke onSuccess closure when fetching fails") {
                    fakeSuccessClosure.verifyCall(withIdentifier: fakeSuccessClosure.idFakeClosure, mode: .never)
                }

            }
        }

    }
}
