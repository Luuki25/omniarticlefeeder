//
// Created by Łukasz Sypniewski on 2019-04-17.
// Copyright (c) 2019 Łukasz Sypniewski. All rights reserved.
//

import Foundation
import Mimus

class FakeClosure: Mock {
    var storage: [RecordedCall] = []
    let idFakeClosure = "idFakeClosure"
    let idFakeClosureWithError = "idFakeClosureWithError"

    func fakeClosure() {
        recordCall(withIdentifier: idFakeClosure)
    }

    func fakeClosureWithError(_ error: Error?) {
        recordCall(withIdentifier: idFakeClosureWithError, arguments: [error])
    }
}
